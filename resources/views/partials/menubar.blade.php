<header class="shop_header_area">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img src="{{asset('img/logo.png')}}" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse container4" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown submenu">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Pets Type <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="nav-item"><a class="nav-link" href="compare.html">Birds</a></li>
                            <li class="nav-item"><a class="nav-link" href="checkout.html">Dogs</a></li>
                            <li class="nav-item"><a class="nav-link" href="register.html">Cats</a></li>
                            <li class="nav-item"><a class="nav-link" href="track.html">Fishes</a></li>
                            <li class="nav-item"><a class="nav-link" href="login.html">Others</a></li>
                        </ul>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">About Us</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact.html">Contact Us</a></li>
                    <li class="nav-item"><a class="nav-link" href="contact.html">Blog</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
{{--<style>--}}
    {{--div.container4 {--}}
        {{--height: 4em;--}}
        {{--/*position: relative*/--}}
    {{--}--}}
    {{--div.container4 ul {--}}
        {{--margin: 0;--}}
        {{--position: absolute;--}}
        {{--top: 50%;--}}
        {{--left: 50%;--}}
        {{--margin-right: -40%;--}}
        {{--transform: translate(-50%, -50%) }--}}
{{--</style>--}}

