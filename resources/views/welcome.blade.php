@extends('layouts.app')
@section('content')
<!--================Feature Add Area =================-->
<section class="feature_add_area">
    <div class="container">
        <div class="row feature_inner">
            <div class="col-lg-4">
                <div class="f_add_item">
                    <div class="f_add_img"><img style="width: 100%; height: 200px !important;" class="img-fluid" src="img/feature-add/f-add-1.jpg" alt=""></div>
                    <div class="f_add_hover">
                        <h4>Best peigon <br />Collection</h4>
                        <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                    </div>
                    <div class="sale">Sale</div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="f_add_item right_dir">
                    <div class="f_add_img"><img style="width: 80%; height: 200px !important;" class="img-fluid" src="img/feature-add/f-add-2.jpg" alt=""></div>
                    <div class="f_add_hover">
                        <h4>Best Birds <br />Collection</h4>
                        <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                    </div>
                    <div class="off">10% off</div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="f_add_item right_dir">
                    <div class="f_add_img"><img style="width: 100%; height: 200px !important;" class="img-fluid" src="img/feature-add/f-add-3.jpg" alt=""></div>
                    <div class="f_add_hover">
                        <h4>Best Birds <br />Collection</h4>
                        <a class="add_btn" href="#">Shop Now <i class="arrow_right"></i></a>
                    </div>
                    <div class="off">10% off</div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--================End Feature Add Area =================-->

<!--================Our Latest Product Area =================-->
<section class="our_latest_product">
    <div class="container">
        <div class="s_m_title">
            <h2>Our Latest Product</h2>
        </div>
        <div class="l_product_slider owl-carousel">
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-1.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Womens Libero</h4>
                        <h5><del>$45.50</del>  $40</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-5.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Oxford Shirt</h4>
                        <h5>$85.50</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-2.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Travel Bags</h4>
                        <h5><del>$45.50</del>  $40</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-3.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>High Heel</h4>
                        <h5><del>$130.50</del>  $110</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-3.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Summer Dress</h4>
                        <h5>$45.05</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-5.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Fossil Watch</h4>
                        <h5>$250.00</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-4.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Nike Shoes</h4>
                        <h5><del>$130</del> $110</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-1.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Ricky Shirt</h4>
                        <h5>$45.05</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Our Latest Product Area =================-->

<!--================Featured Product Area =================-->
<section class="our_latest_product">
    <div class="container">
        <div class="s_m_title">
            <h2>Our Featured Product</h2>
        </div>
        <div class="l_product_slider owl-carousel">
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-1.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Womens Libero</h4>
                        <h5><del>$45.50</del>  $40</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-5.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Oxford Shirt</h4>
                        <h5>$85.50</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-2.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Travel Bags</h4>
                        <h5><del>$45.50</del>  $40</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-3.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>High Heel</h4>
                        <h5><del>$130.50</del>  $110</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-3.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Summer Dress</h4>
                        <h5>$45.05</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-5.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Fossil Watch</h4>
                        <h5>$250.00</h5>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-4.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Nike Shoes</h4>
                        <h5><del>$130</del> $110</h5>
                    </div>
                </div>
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img style="height: 140px !important;" src="img/product/l-product-1.jpg" alt="">
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                            <li><a class="add_cart_btn" href="#">Add To Cart</a></li>
                            <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                        </ul>
                        <h4>Ricky Shirt</h4>
                        <h5>$45.05</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Featured Product Area =================-->

<!--================Form Blog Area =================-->
<section class="from_blog_area">
    <div class="container">
        <div class="from_blog_inner">
            <div class="c_main_title">
                <h2>From The Blog</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="from_blog_item">
                        <img class="img-fluid" src="img/blog/from-blog/f-blog-1.jpg" alt="">
                        <div class="f_blog_text">
                            <h5>fashion</h5>
                            <p>Neque porro quisquam est qui dolorem ipsum</p>
                            <h6>21.09.2017</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="from_blog_item">
                        <img class="img-fluid" src="img/blog/from-blog/f-blog-2.jpg" alt="">
                        <div class="f_blog_text">
                            <h5>fashion</h5>
                            <p>Neque porro quisquam est qui dolorem ipsum</p>
                            <h6>21.09.2017</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="from_blog_item">
                        <img class="img-fluid" src="img/blog/from-blog/f-blog-3.jpg" alt="">
                        <div class="f_blog_text">
                            <h5>fashion</h5>
                            <p>Neque porro quisquam est qui dolorem ipsum</p>
                            <h6>21.09.2017</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Form Blog Area =================-->
@endsection
