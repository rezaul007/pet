<div class="header_top_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="top_header_left">
                    <div class="selector">
                        <select class="language_drop" name="countries" id="countries" style="width:300px;">
                            <option value='yt' data-image="img/icon/flag-1.png" data-imagecss="flag yt" data-title="English">English</option>
                            <option value='yu' data-image="img/icon/Bangladesh-Flag.png" data-imagecss="flag yu" data-title="Bangladesh">Bangla</option>
                        </select>
                    </div>
                    {{--<select class="selectpicker usd_select">--}}
                        {{--<option>USD</option>--}}
                        {{--<option>$</option>--}}
                        {{--<option>$</option>--}}
                    {{--</select>--}}
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" aria-label="Search">
                        <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button"><i class="icon-magnifier"></i></button>
                                </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top_header_middle">
                    <a href="#"><i class="fa fa-phone"></i> Call Us: <span>+84 987 654 321</span></a>
                    <a href="#"><i class="fa fa-envelope"></i> Email: <span>support@yourdomain.com</span></a>
                    <img src="img/logo.png" alt="">
                </div>
            </div>
            <div class="col-lg-3">
                <div class="top_right_header">
                    <ul class="header_social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    <ul class="top_right">
                        <li class="user"><a href="#"><i class="icon-user icons"></i></a></li>
                        <li class="user"><a href="#"><i class="icon-user icons"></i></a></li>
                        {{--<li class="cart"><a href="#"><i class="icon-handbag icons"></i></a></li>--}}
                        {{--<li class="h_price">--}}
                            {{--<select class="selectpicker">--}}
                                {{--<option>$0.00</option>--}}
                                {{--<option>$0.00</option>--}}
                                {{--<option>$0.00</option>--}}
                            {{--</select>--}}
                        {{--</li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

